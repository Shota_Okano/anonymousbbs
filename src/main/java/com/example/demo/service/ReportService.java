package com.example.demo.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// Report [show]
	public List<Report> findAllReport() {
		return reportRepository.findAllByOrderByUpdatedDateDesc();
	}

	public List<Report> findAllReport(Timestamp since, Timestamp until) {
		return reportRepository.findByCreatedDateBetween(since, until);
	}

	// Report [new]
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// 主キー検索メソッド
	public Report findById(Integer id) {
		return reportRepository.findById(id).get();
	}

	public void deleteReport(int reportId) {
		Report reportData = findById(reportId);
		reportRepository.delete(reportData);
	}

	public Report findReport(int reportId) {
		Report reportData = findById(reportId);
		return reportData;
	}

	// Report [update]
	public void updateReport(Report report) {
		reportRepository.save(report);
	}
}

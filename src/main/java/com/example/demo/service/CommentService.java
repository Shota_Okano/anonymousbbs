package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	// report_id に紐づく comment 全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAllByOrderByUpdatedDateDesc();
	}

	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	// 主キー検索メソッド
	public Comment findById(Integer id) {
		return commentRepository.findById(id).get();
	}

	public void deleteComment(Integer commentId) {
		Comment commentData = findById(commentId);
		commentRepository.delete(commentData);
	}

	public Comment findComment(int commentId) {
		Comment commentData = findById(commentId);
		return commentData;
	}

	// Comment[update]
	public void updateComment(Comment comment) {
		commentRepository.save(comment);
	}
}

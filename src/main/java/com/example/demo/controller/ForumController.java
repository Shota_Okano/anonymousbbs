package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表⽰画⾯
//	@GetMapping
//	public ModelAndView top() {
//		ModelAndView mav = new ModelAndView();
//		// 投稿を全件取得
//		List<Report> contentData = reportService.findAllReport();
//
//		// 画⾯遷移先を指定
//		mav.setViewName("/top");
//
//		// 投稿データオブジェクトを保管
//		mav.addObject("contents", contentData);
//		return mav;
//	}

	@GetMapping
	public ModelAndView top(@RequestParam(value = "startDate", required = false) String startDate, @RequestParam(value = "endDate", required = false) String endDate) {
		ModelAndView mav = new ModelAndView();
		Timestamp since = Timestamp.valueOf("2020-01-01 00:00:00");;
		Timestamp until = new Timestamp(System.currentTimeMillis());
		List<Report> contentData = null;

		if(!ObjectUtils.isEmpty(startDate)) {
			since = Timestamp.valueOf(startDate + " 00:00:00");
		}

		if(!ObjectUtils.isEmpty(endDate)) {
			until = Timestamp.valueOf(endDate + " 23:59:59");
		}
		// 投稿を全件取得
		if(!ObjectUtils.isEmpty(endDate) || !ObjectUtils.isEmpty(endDate)) {
			contentData = reportService.findAllReport(since, until);
		} else {
			contentData = reportService.findAllReport();
		}
		// 画⾯遷移先を指定
		mav.setViewName("/top");

		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		return mav;
	}

// report関連
	// report投稿 [GET]
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のEntityを用意
		Report report = new Report();

		mav.setViewName("/new");
		//準備した空のEntityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// report投稿 [POST]
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		//投稿をテーブルに格納。
		reportService.saveReport(report);

		// rootへRedirect
		return new ModelAndView("redirect:/");
	}

	// report削除 [POST]
	@PostMapping("/delete")
	public ModelAndView delete(@RequestParam("reportId") String report) {
		int reportId = Integer.parseInt(report);
		reportService.deleteReport(reportId);

		return new ModelAndView("redirect:/");
	}

	// report編集 [GET]
	@GetMapping("/edit")
	public ModelAndView edit(@RequestParam("reportId") String report) {
		ModelAndView mav = new ModelAndView();
		int reportId = Integer.parseInt(report);

		Report reportData = reportService.findReport(reportId);

		mav.setViewName("/edit");
		mav.addObject("content", reportData);
		return mav;
	}

	// report編集 [POST]
	@PostMapping("/edit")
	public ModelAndView edit(@ModelAttribute("formModel") Report report, @RequestParam("reportId") String reportId) {
		report.setId(Integer.parseInt(reportId));
		reportService.updateReport(report);

		return new ModelAndView("redirect:/");
	}

	// report詳細 {GET]
	@GetMapping("/show")
	public ModelAndView show(@RequestParam("reportId") String reportId) {
		ModelAndView mav = new ModelAndView();

		// report, comment 取得
		Report report = reportService.findReport(Integer.parseInt(reportId));
		List<Comment> comments = commentService.findAllComment();

		mav.setViewName("/show");
		mav.addObject("content", report);
		mav.addObject("comments", comments);

		return mav;
	}

// Comment関連
	// comment投稿 [GET]
	@GetMapping("/comment")
	public ModelAndView newComment(@RequestParam("reportId") String reportId) {

		Report reportData = reportService.findReport(Integer.parseInt(reportId));
		ModelAndView mav = new ModelAndView();
		Comment comment = new Comment();

		mav.setViewName("/comment");
		mav.addObject("reportData", reportData);
		mav.addObject("formModel", comment);
		return mav;
	}
	// comment投稿 [POST]
	@PostMapping("/comment")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment comment, @RequestParam("reportId") String reportId) {

		Report report = reportService.findById(Integer.parseInt(reportId));

		comment.setReportId(Integer.parseInt(reportId));
		report.setUpdatedDate(new Timestamp(System.currentTimeMillis()));

		commentService.saveComment(comment);
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	// comment削除
	@GetMapping("/deleteComment")
	public ModelAndView deleteComment(@RequestParam("commentId") String comment) {
		int commentId = Integer.parseInt(comment);
		commentService.deleteComment(commentId);
		return new ModelAndView("redirect:/");
	}

	// comment編集 [GET]
	@GetMapping("/editComment")
	public ModelAndView editComment(@RequestParam("commentId") String comment, @RequestParam("reportId") String reportId) {
		ModelAndView mav = new ModelAndView();
		int commentId = Integer.parseInt(comment);

		Comment commentData = commentService.findComment(commentId);

		mav.setViewName("/editComment");
		mav.addObject("reportId", Integer.valueOf(reportId));
		mav.addObject("comment", commentData);
		return mav;
	}

	@PostMapping("/editComment")
	public ModelAndView editComment(@ModelAttribute("formModel") Comment comment, @RequestParam("commentId") String commentId, @RequestParam("reportId") String reportId) {
		comment.setId(Integer.parseInt(commentId));
		comment.setReportId(Integer.parseInt(reportId));
		commentService.updateComment(comment);

		return new ModelAndView("redirect:/");
	}
}
